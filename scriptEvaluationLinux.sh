# Utilisation : 
# bash script $nomEleve
# Fichier de retour généré dans le home de l'utilisateur courant.
cd
mkdir retour-guillaume
cd retour-guillaume
scp root@10.10.10.1:/etc/apache2/sites-available/* ./
scp root@10.10.10.1:/etc/network/interfaces ./
scp root@10.10.10.1:/root/.ssh/authorized_keys ./
scp root@10.10.10.1:/etc/ssh/sshd_config ./
ssh root@10.10.10.1 'find /var/www/' > ./VarWww.txt
ssh root@10.10.10.1 'docker ps -a' > dockerPs.txt
ssh root@10.10.10.1 'docker images' > dockerImages.txt
ssh root@10.10.10.1 'find /home -iname dockerfile -exec cat {} \;' > Dockerfile
ssh root@10.10.10.1 'find /home -iname docker-compose.yml -exec cat {} \;' > docker-compose.yml
ssh root@10.10.10.1 'mysqldump --all-databases' > ./exportBaseSql.sql
tar -cvzf ~/$1.gmsi.tar.gz ~/retour-guillaume
